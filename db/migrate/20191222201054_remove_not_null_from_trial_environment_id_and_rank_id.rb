class RemoveNotNullFromTrialEnvironmentIdAndRankId < ActiveRecord::Migration[5.0]
  def change
    change_column :trials, :rank_id, :integer, :null => true
    change_column :trials, :environment_id, :integer, :null => true
  end
end
