# ZAKI README

## How to use
* manager guide [PL]: https://goo.gl/Zvsmbp
* user guide [PL]: https://goo.gl/jEb3Au



## Managing production

1. Push variables from config/application.yml to production
```sh
foo@bar:~$ bundle exec cap production setup
```
2. Puma lifecycle
```sh
foo@bar:~$ cap production puma:status
foo@bar:~$ cap production puma:start
foo@bar:~$ cap production puma:restart
foo@bar:~$ cap production puma:stop
foo@bar:~$ cap production deploy # heads up! it should be managed by CI
```

## Development and contribution

**Details about contributing you can find in the following file: [CONTRIBUTING.md](https://gitlab.com/modorg/zaki/zaki/blob/master/CONTRIBUTING.md)**

- Full cluster installation: https://gitlab.com/modorg/techno/-/wikis/modorg-cluster-installation
- With docker configured to start single application you need to just run `docker compose up`.
 Hovewer it is not very helpful because apps need to have domain routing configured.

### General tips:

1. To have Google Drive service enabled for development purpouses you need to have .env file with GOOGLE_CLIENT_ID variable set in zaki_cluster root directory.
You can get this file from: asterdrak@gmail.com.
2. To open trial as non logged user, you need to have trial url and access key (clear text or digest).
When you open trial page (for example in private window or different browser) you need to paste access key (clear text or digest) to a form.
Then you'll be authenticated as trial creator.
