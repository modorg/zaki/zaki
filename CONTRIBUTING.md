# Contributing guide

The contribution (help with application development) is very welcome under the following rules and conditions.

## Rules and conditions

- any work should come under issue board:  https://gitlab.com/modorg/zaki/zaki/-/boards
- at first issues from *ToDo* pipeline should be selected (full description of pipelines and labels is here: https://gitlab.com/modorg/zaki/zaki/-/labels)
- if there is to suitable issues it *ToDo* we can take issues from backlog
- we do not touch issues from *IceBox*
- is we start working on an issue we should assign ourselves to that and move it to *Doing* pipeline
- issues should be created via service desk (incoming+modorg-zaki-zaki-4187901-issue-@incoming.gitlab.com) or directly in https://gitlab.com/modorg/zaki/zaki/issues
- we do not start two issues at the same time, the only exception is when issue is waiting for code review acceptance
- when we quit issue (with no code chages) we should removes assignement and restore issue to previouse pipeline

## Issue Estimation

| Category                  | Weight | Description |
| ---                            | - | ---         |
| Easy                           | 1 | Very simple, small changes in one app and at most two layers of the app (e.g. model + controller / controller + view). |
| Easy+ / Easy with problems     | 2 | Easy+ - single app 3 layers, small changes. Rather enhancements than stories. |
| Easy+ with problems            | 3 | 'With problems' means that problem happened to be unexpectedly big. |
| Medium                         | 4 | Single app, multiple layers, bigger changes. Rather stories or big improvements than small enhacements.|
| Medium+ / Medium with problems | 5 | Medium+ requires some conceptual work (problem solving) or more advanced techniques - e.g. consume existing API from another app. |
| Medium+ with problems          | 6 | 'With problems' means that problem happened to be unexpectedly big. |
| Hard                           | 7 | Great deal of conceptual work and feature implementation in one app or development spread through multiple apps. |
| Hard+ / Hard with problems     | 8 | Extraordinarily big, biggest possible to be put in one issue. |
| Hard+ with problems            | 9 | 'With problems' means that problem happened to be unexpectedly big. |

## Work with issue

**The following steps assume, that you have already installed and cofigured zaki cluster and working environment: https://gitlab.com/modorg/techno/-/wikis/modorg-cluster-installation**

1. [Select issue](https://gitlab.com/modorg/zaki/zaki/-/boards) you would like to work on (must be *ToDo* or *Backlog* and unassigned).
2. Move issue to *Doing* pipeline and asssign issue to yourself.
3. If issue is dedicated to application other to zaki (e.g. formsub), move it to appropriate repository (issue page -> right bottom of the page).
4. In the issue page click button: *Create merge request*
5. Open created merge request and copy brach name (e.g. 71-name-of-issue)
6. Go to zaki cluster root dir:

```sh
cd zaki # change working directory to ZAKI or another app directory
git fetch # check remote repository for updates
git checkout 71-name-of-issue # checkout your branch
cd .. # get back to root directory
docker-compose up # start application, ctrl-c to stop
# --------------------------------------
# work on issue, change code and test it
# changes should include automatic tests
# --------------------------------------
# when finished:
cd zaki # change working directory to ZAKI or another app directory
git diff # show latest changes
git add . # add all to git tracking
git diff HEAD # show all changes since last commit
git commit -m "HERE YOU ADD YOUR COMMIT MESSAGE, CHANGE IT!!!!" # commit all tracked changes
git push # push changes (commits) to remote remository
```
7. When you finish working on an issue move it to *Testing* pipeline and remove WIP status from merge request.
8. Request code review.
9. Fix anything what is mentioned in comments.

