# frozen_string_literal: true
require 'rails_helper'

RSpec.describe Committee, type: :model do
  it { is_expected.to validate_presence_of :name }
  # it { is_expected.to validate_uniqueness_of :name }
  # it { is_expected.to validate_presence_of :stateman }
  it { is_expected.to validate_presence_of :min_trial_tasks_count }

  it { is_expected.to have_many(:trials) }
  it { is_expected.to have_one(:stateman) }
  it { is_expected.to have_many(:ranks) }
  it { is_expected.to have_many(:environments) }
  it { is_expected.to have_many(:comments) }
  it { is_expected.to respond_to :redirection= }

  it 'triggers create_stateman_resources on create' do
    committee = Committee.allocate
    expect(committee).to receive(:create_stateman_resources)
    committee.send(:initialize)
    committee.name = 'name1'
    committee.save
  end

  describe '#redirection' do
    it 'sets up redirection non nil' do
      subject.redirection = 'some_url'

      expect(subject.redirection).to eq('some_url')
    end

    it 'sets up redirection nil' do
      subject.redirection = nil

      expect(subject.redirection).to eq(subject)
    end
  end

  describe '#drive_configured?' do
    it do
      subject.drive_root = 'sth'
      subject.drive_token = 'sth'

      expect(subject.drive_configured?).to be_truthy
    end

    it do
      subject.drive_root = 'sth'
      subject.drive_token = nil

      expect(subject.drive_configured?).to be_falsy
    end

    it do
      subject.drive_root = nil
      subject.drive_token = 'sth'

      expect(subject.drive_configured?).to be_falsy
    end

    it do
      subject.drive_root = nil
      subject.drive_token = nil

      expect(subject.drive_configured?).to be_falsy
    end
  end
end
